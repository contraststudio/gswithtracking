// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NDIIOEDITOR_NDIMediaSenderFactory_generated_h
#error "NDIMediaSenderFactory.generated.h already included, missing '#pragma once' in NDIMediaSenderFactory.h"
#endif
#define NDIIOEDITOR_NDIMediaSenderFactory_generated_h

#define NDIExamples_Plugins_NDIIOPlugin_Source_Editor_Public_Factories_NDIMediaSenderFactory_h_22_SPARSE_DATA
#define NDIExamples_Plugins_NDIIOPlugin_Source_Editor_Public_Factories_NDIMediaSenderFactory_h_22_RPC_WRAPPERS
#define NDIExamples_Plugins_NDIIOPlugin_Source_Editor_Public_Factories_NDIMediaSenderFactory_h_22_RPC_WRAPPERS_NO_PURE_DECLS
#define NDIExamples_Plugins_NDIIOPlugin_Source_Editor_Public_Factories_NDIMediaSenderFactory_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNDIMediaSenderFactory(); \
	friend struct Z_Construct_UClass_UNDIMediaSenderFactory_Statics; \
public: \
	DECLARE_CLASS(UNDIMediaSenderFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NDIIOEditor"), NO_API) \
	DECLARE_SERIALIZER(UNDIMediaSenderFactory)


#define NDIExamples_Plugins_NDIIOPlugin_Source_Editor_Public_Factories_NDIMediaSenderFactory_h_22_INCLASS \
private: \
	static void StaticRegisterNativesUNDIMediaSenderFactory(); \
	friend struct Z_Construct_UClass_UNDIMediaSenderFactory_Statics; \
public: \
	DECLARE_CLASS(UNDIMediaSenderFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NDIIOEditor"), NO_API) \
	DECLARE_SERIALIZER(UNDIMediaSenderFactory)


#define NDIExamples_Plugins_NDIIOPlugin_Source_Editor_Public_Factories_NDIMediaSenderFactory_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNDIMediaSenderFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNDIMediaSenderFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNDIMediaSenderFactory); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNDIMediaSenderFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNDIMediaSenderFactory(UNDIMediaSenderFactory&&); \
	NO_API UNDIMediaSenderFactory(const UNDIMediaSenderFactory&); \
public:


#define NDIExamples_Plugins_NDIIOPlugin_Source_Editor_Public_Factories_NDIMediaSenderFactory_h_22_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNDIMediaSenderFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNDIMediaSenderFactory(UNDIMediaSenderFactory&&); \
	NO_API UNDIMediaSenderFactory(const UNDIMediaSenderFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNDIMediaSenderFactory); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNDIMediaSenderFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNDIMediaSenderFactory)


#define NDIExamples_Plugins_NDIIOPlugin_Source_Editor_Public_Factories_NDIMediaSenderFactory_h_22_PRIVATE_PROPERTY_OFFSET
#define NDIExamples_Plugins_NDIIOPlugin_Source_Editor_Public_Factories_NDIMediaSenderFactory_h_19_PROLOG
#define NDIExamples_Plugins_NDIIOPlugin_Source_Editor_Public_Factories_NDIMediaSenderFactory_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	NDIExamples_Plugins_NDIIOPlugin_Source_Editor_Public_Factories_NDIMediaSenderFactory_h_22_PRIVATE_PROPERTY_OFFSET \
	NDIExamples_Plugins_NDIIOPlugin_Source_Editor_Public_Factories_NDIMediaSenderFactory_h_22_SPARSE_DATA \
	NDIExamples_Plugins_NDIIOPlugin_Source_Editor_Public_Factories_NDIMediaSenderFactory_h_22_RPC_WRAPPERS \
	NDIExamples_Plugins_NDIIOPlugin_Source_Editor_Public_Factories_NDIMediaSenderFactory_h_22_INCLASS \
	NDIExamples_Plugins_NDIIOPlugin_Source_Editor_Public_Factories_NDIMediaSenderFactory_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define NDIExamples_Plugins_NDIIOPlugin_Source_Editor_Public_Factories_NDIMediaSenderFactory_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	NDIExamples_Plugins_NDIIOPlugin_Source_Editor_Public_Factories_NDIMediaSenderFactory_h_22_PRIVATE_PROPERTY_OFFSET \
	NDIExamples_Plugins_NDIIOPlugin_Source_Editor_Public_Factories_NDIMediaSenderFactory_h_22_SPARSE_DATA \
	NDIExamples_Plugins_NDIIOPlugin_Source_Editor_Public_Factories_NDIMediaSenderFactory_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	NDIExamples_Plugins_NDIIOPlugin_Source_Editor_Public_Factories_NDIMediaSenderFactory_h_22_INCLASS_NO_PURE_DECLS \
	NDIExamples_Plugins_NDIIOPlugin_Source_Editor_Public_Factories_NDIMediaSenderFactory_h_22_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class NDIMediaSenderFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NDIIOEDITOR_API UClass* StaticClass<class UNDIMediaSenderFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID NDIExamples_Plugins_NDIIOPlugin_Source_Editor_Public_Factories_NDIMediaSenderFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
