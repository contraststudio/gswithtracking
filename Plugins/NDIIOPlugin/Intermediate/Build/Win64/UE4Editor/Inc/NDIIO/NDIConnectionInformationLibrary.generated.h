// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FNDIConnectionInformation;
#ifdef NDIIO_NDIConnectionInformationLibrary_generated_h
#error "NDIConnectionInformationLibrary.generated.h already included, missing '#pragma once' in NDIConnectionInformationLibrary.h"
#endif
#define NDIIO_NDIConnectionInformationLibrary_generated_h

#define NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_Objects_Libraries_NDIConnectionInformationLibrary_h_20_SPARSE_DATA
#define NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_Objects_Libraries_NDIConnectionInformationLibrary_h_20_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execK2_NDIConnectionInformation_Reset); \
	DECLARE_FUNCTION(execK2_NDIConnectionInformation_IsValid); \
	DECLARE_FUNCTION(execK2_Compare_Not_NDIConnectionInformation); \
	DECLARE_FUNCTION(execK2_Compare_NDIConnectionInformation);


#define NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_Objects_Libraries_NDIConnectionInformationLibrary_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execK2_NDIConnectionInformation_Reset); \
	DECLARE_FUNCTION(execK2_NDIConnectionInformation_IsValid); \
	DECLARE_FUNCTION(execK2_Compare_Not_NDIConnectionInformation); \
	DECLARE_FUNCTION(execK2_Compare_NDIConnectionInformation);


#define NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_Objects_Libraries_NDIConnectionInformationLibrary_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNDIConnectionInformationLibrary(); \
	friend struct Z_Construct_UClass_UNDIConnectionInformationLibrary_Statics; \
public: \
	DECLARE_CLASS(UNDIConnectionInformationLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NDIIO"), NO_API) \
	DECLARE_SERIALIZER(UNDIConnectionInformationLibrary)


#define NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_Objects_Libraries_NDIConnectionInformationLibrary_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUNDIConnectionInformationLibrary(); \
	friend struct Z_Construct_UClass_UNDIConnectionInformationLibrary_Statics; \
public: \
	DECLARE_CLASS(UNDIConnectionInformationLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NDIIO"), NO_API) \
	DECLARE_SERIALIZER(UNDIConnectionInformationLibrary)


#define NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_Objects_Libraries_NDIConnectionInformationLibrary_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNDIConnectionInformationLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNDIConnectionInformationLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNDIConnectionInformationLibrary); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNDIConnectionInformationLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNDIConnectionInformationLibrary(UNDIConnectionInformationLibrary&&); \
	NO_API UNDIConnectionInformationLibrary(const UNDIConnectionInformationLibrary&); \
public:


#define NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_Objects_Libraries_NDIConnectionInformationLibrary_h_20_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNDIConnectionInformationLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNDIConnectionInformationLibrary(UNDIConnectionInformationLibrary&&); \
	NO_API UNDIConnectionInformationLibrary(const UNDIConnectionInformationLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNDIConnectionInformationLibrary); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNDIConnectionInformationLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNDIConnectionInformationLibrary)


#define NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_Objects_Libraries_NDIConnectionInformationLibrary_h_20_PRIVATE_PROPERTY_OFFSET
#define NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_Objects_Libraries_NDIConnectionInformationLibrary_h_17_PROLOG
#define NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_Objects_Libraries_NDIConnectionInformationLibrary_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_Objects_Libraries_NDIConnectionInformationLibrary_h_20_PRIVATE_PROPERTY_OFFSET \
	NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_Objects_Libraries_NDIConnectionInformationLibrary_h_20_SPARSE_DATA \
	NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_Objects_Libraries_NDIConnectionInformationLibrary_h_20_RPC_WRAPPERS \
	NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_Objects_Libraries_NDIConnectionInformationLibrary_h_20_INCLASS \
	NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_Objects_Libraries_NDIConnectionInformationLibrary_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_Objects_Libraries_NDIConnectionInformationLibrary_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_Objects_Libraries_NDIConnectionInformationLibrary_h_20_PRIVATE_PROPERTY_OFFSET \
	NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_Objects_Libraries_NDIConnectionInformationLibrary_h_20_SPARSE_DATA \
	NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_Objects_Libraries_NDIConnectionInformationLibrary_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_Objects_Libraries_NDIConnectionInformationLibrary_h_20_INCLASS_NO_PURE_DECLS \
	NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_Objects_Libraries_NDIConnectionInformationLibrary_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NDIIO_API UClass* StaticClass<class UNDIConnectionInformationLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_Objects_Libraries_NDIConnectionInformationLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
