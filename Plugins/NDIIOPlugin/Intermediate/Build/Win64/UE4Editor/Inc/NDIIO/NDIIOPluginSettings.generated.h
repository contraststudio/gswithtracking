// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NDIIO_NDIIOPluginSettings_generated_h
#error "NDIIOPluginSettings.generated.h already included, missing '#pragma once' in NDIIOPluginSettings.h"
#endif
#define NDIIO_NDIIOPluginSettings_generated_h

#define NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_NDIIOPluginSettings_h_23_SPARSE_DATA
#define NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_NDIIOPluginSettings_h_23_RPC_WRAPPERS
#define NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_NDIIOPluginSettings_h_23_RPC_WRAPPERS_NO_PURE_DECLS
#define NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_NDIIOPluginSettings_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNDIIOPluginSettings(); \
	friend struct Z_Construct_UClass_UNDIIOPluginSettings_Statics; \
public: \
	DECLARE_CLASS(UNDIIOPluginSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/NDIIO"), NO_API) \
	DECLARE_SERIALIZER(UNDIIOPluginSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_NDIIOPluginSettings_h_23_INCLASS \
private: \
	static void StaticRegisterNativesUNDIIOPluginSettings(); \
	friend struct Z_Construct_UClass_UNDIIOPluginSettings_Statics; \
public: \
	DECLARE_CLASS(UNDIIOPluginSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/NDIIO"), NO_API) \
	DECLARE_SERIALIZER(UNDIIOPluginSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_NDIIOPluginSettings_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNDIIOPluginSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNDIIOPluginSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNDIIOPluginSettings); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNDIIOPluginSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNDIIOPluginSettings(UNDIIOPluginSettings&&); \
	NO_API UNDIIOPluginSettings(const UNDIIOPluginSettings&); \
public:


#define NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_NDIIOPluginSettings_h_23_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNDIIOPluginSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNDIIOPluginSettings(UNDIIOPluginSettings&&); \
	NO_API UNDIIOPluginSettings(const UNDIIOPluginSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNDIIOPluginSettings); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNDIIOPluginSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNDIIOPluginSettings)


#define NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_NDIIOPluginSettings_h_23_PRIVATE_PROPERTY_OFFSET
#define NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_NDIIOPluginSettings_h_20_PROLOG
#define NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_NDIIOPluginSettings_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_NDIIOPluginSettings_h_23_PRIVATE_PROPERTY_OFFSET \
	NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_NDIIOPluginSettings_h_23_SPARSE_DATA \
	NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_NDIIOPluginSettings_h_23_RPC_WRAPPERS \
	NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_NDIIOPluginSettings_h_23_INCLASS \
	NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_NDIIOPluginSettings_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_NDIIOPluginSettings_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_NDIIOPluginSettings_h_23_PRIVATE_PROPERTY_OFFSET \
	NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_NDIIOPluginSettings_h_23_SPARSE_DATA \
	NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_NDIIOPluginSettings_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_NDIIOPluginSettings_h_23_INCLASS_NO_PURE_DECLS \
	NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_NDIIOPluginSettings_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NDIIO_API UClass* StaticClass<class UNDIIOPluginSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID NDIExamples_Plugins_NDIIOPlugin_Source_Core_Public_NDIIOPluginSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
