// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.IO;


public class RenderStream : ModuleRules
{
    public RenderStream(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "Sockets", "Networking", "MediaIOCore", "MediaUtils", "InputCore", "UMG" });
        PrivateDependencyModuleNames.AddRange(new string[] { "CoreUObject", "Engine", "Slate", "SlateCore", "CinematicCamera", "RHI", "D3D11RHI", "D3D12RHI", "RenderCore", "Projects", "Json", "JsonUtilities" });

        PrivateIncludePaths.AddRange(
            new string[]
            {
                Path.Combine(EngineDirectory, "Source/Runtime/D3D12RHI/Private"),
                Path.Combine(EngineDirectory, "Source/Runtime/D3D12RHI/Private/Windows"),
                Path.Combine(EngineDirectory, "Source/ThirdParty/Windows/D3DX12/Include")
            });

        DynamicallyLoadedModuleNames.AddRange(new string[] { });

        AddEngineThirdPartyPrivateStaticDependencies(Target, "DX11");
        AddEngineThirdPartyPrivateStaticDependencies(Target, "DX12");
        //AddEngineThirdPartyPrivateStaticDependencies(Target, "NVAPI");
        //AddEngineThirdPartyPrivateStaticDependencies(Target, "AMD_AGS");
        //AddEngineThirdPartyPrivateStaticDependencies(Target, "NVAftermath");
        //AddEngineThirdPartyPrivateStaticDependencies(Target, "IntelMetricsDiscovery");	}
    }
}
