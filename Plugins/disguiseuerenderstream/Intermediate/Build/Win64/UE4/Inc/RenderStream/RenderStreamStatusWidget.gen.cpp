// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RenderStream/Public/RenderStreamStatusWidget.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRenderStreamStatusWidget() {}
// Cross Module References
	RENDERSTREAM_API UClass* Z_Construct_UClass_URenderStreamStatusWidget_NoRegister();
	RENDERSTREAM_API UClass* Z_Construct_UClass_URenderStreamStatusWidget();
	UMG_API UClass* Z_Construct_UClass_UUserWidget();
	UPackage* Z_Construct_UPackage__Script_RenderStream();
// End Cross Module References
	void URenderStreamStatusWidget::StaticRegisterNativesURenderStreamStatusWidget()
	{
	}
	UClass* Z_Construct_UClass_URenderStreamStatusWidget_NoRegister()
	{
		return URenderStreamStatusWidget::StaticClass();
	}
	struct Z_Construct_UClass_URenderStreamStatusWidget_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URenderStreamStatusWidget_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UUserWidget,
		(UObject* (*)())Z_Construct_UPackage__Script_RenderStream,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URenderStreamStatusWidget_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "RenderStreamStatusWidget.h" },
		{ "ModuleRelativePath", "Public/RenderStreamStatusWidget.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_URenderStreamStatusWidget_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URenderStreamStatusWidget>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URenderStreamStatusWidget_Statics::ClassParams = {
		&URenderStreamStatusWidget::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00B010A0u,
		METADATA_PARAMS(Z_Construct_UClass_URenderStreamStatusWidget_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URenderStreamStatusWidget_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URenderStreamStatusWidget()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URenderStreamStatusWidget_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URenderStreamStatusWidget, 1022166378);
	template<> RENDERSTREAM_API UClass* StaticClass<URenderStreamStatusWidget>()
	{
		return URenderStreamStatusWidget::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URenderStreamStatusWidget(Z_Construct_UClass_URenderStreamStatusWidget, &URenderStreamStatusWidget::StaticClass, TEXT("/Script/RenderStream"), TEXT("URenderStreamStatusWidget"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URenderStreamStatusWidget);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
