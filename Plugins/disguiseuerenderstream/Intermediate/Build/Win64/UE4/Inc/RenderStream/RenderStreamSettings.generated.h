// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef RENDERSTREAM_RenderStreamSettings_generated_h
#error "RenderStreamSettings.generated.h already included, missing '#pragma once' in RenderStreamSettings.h"
#endif
#define RENDERSTREAM_RenderStreamSettings_generated_h

#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamSettings_h_15_SPARSE_DATA
#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamSettings_h_15_RPC_WRAPPERS
#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamSettings_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamSettings_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURenderStreamSettings(); \
	friend struct Z_Construct_UClass_URenderStreamSettings_Statics; \
public: \
	DECLARE_CLASS(URenderStreamSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/RenderStream"), NO_API) \
	DECLARE_SERIALIZER(URenderStreamSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamSettings_h_15_INCLASS \
private: \
	static void StaticRegisterNativesURenderStreamSettings(); \
	friend struct Z_Construct_UClass_URenderStreamSettings_Statics; \
public: \
	DECLARE_CLASS(URenderStreamSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/RenderStream"), NO_API) \
	DECLARE_SERIALIZER(URenderStreamSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamSettings_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URenderStreamSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URenderStreamSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URenderStreamSettings); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URenderStreamSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URenderStreamSettings(URenderStreamSettings&&); \
	NO_API URenderStreamSettings(const URenderStreamSettings&); \
public:


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamSettings_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URenderStreamSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URenderStreamSettings(URenderStreamSettings&&); \
	NO_API URenderStreamSettings(const URenderStreamSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URenderStreamSettings); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URenderStreamSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URenderStreamSettings)


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamSettings_h_15_PRIVATE_PROPERTY_OFFSET
#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamSettings_h_12_PROLOG
#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamSettings_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamSettings_h_15_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamSettings_h_15_SPARSE_DATA \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamSettings_h_15_RPC_WRAPPERS \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamSettings_h_15_INCLASS \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamSettings_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamSettings_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamSettings_h_15_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamSettings_h_15_SPARSE_DATA \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamSettings_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamSettings_h_15_INCLASS_NO_PURE_DECLS \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamSettings_h_15_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class RenderStreamSettings."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RENDERSTREAM_API UClass* StaticClass<class URenderStreamSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
