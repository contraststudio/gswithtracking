// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef RENDERSTREAM_RenderStreamMediaOutput_generated_h
#error "RenderStreamMediaOutput.generated.h already included, missing '#pragma once' in RenderStreamMediaOutput.h"
#endif
#define RENDERSTREAM_RenderStreamMediaOutput_generated_h

#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaOutput_h_44_SPARSE_DATA
#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaOutput_h_44_RPC_WRAPPERS
#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaOutput_h_44_RPC_WRAPPERS_NO_PURE_DECLS
#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaOutput_h_44_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURenderStreamMediaOutput(); \
	friend struct Z_Construct_UClass_URenderStreamMediaOutput_Statics; \
public: \
	DECLARE_CLASS(URenderStreamMediaOutput, UMediaOutput, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RenderStream"), NO_API) \
	DECLARE_SERIALIZER(URenderStreamMediaOutput)


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaOutput_h_44_INCLASS \
private: \
	static void StaticRegisterNativesURenderStreamMediaOutput(); \
	friend struct Z_Construct_UClass_URenderStreamMediaOutput_Statics; \
public: \
	DECLARE_CLASS(URenderStreamMediaOutput, UMediaOutput, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RenderStream"), NO_API) \
	DECLARE_SERIALIZER(URenderStreamMediaOutput)


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaOutput_h_44_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URenderStreamMediaOutput(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URenderStreamMediaOutput) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URenderStreamMediaOutput); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URenderStreamMediaOutput); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URenderStreamMediaOutput(URenderStreamMediaOutput&&); \
	NO_API URenderStreamMediaOutput(const URenderStreamMediaOutput&); \
public:


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaOutput_h_44_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URenderStreamMediaOutput(URenderStreamMediaOutput&&); \
	NO_API URenderStreamMediaOutput(const URenderStreamMediaOutput&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URenderStreamMediaOutput); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URenderStreamMediaOutput); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(URenderStreamMediaOutput)


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaOutput_h_44_PRIVATE_PROPERTY_OFFSET
#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaOutput_h_41_PROLOG
#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaOutput_h_44_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaOutput_h_44_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaOutput_h_44_SPARSE_DATA \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaOutput_h_44_RPC_WRAPPERS \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaOutput_h_44_INCLASS \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaOutput_h_44_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaOutput_h_44_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaOutput_h_44_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaOutput_h_44_SPARSE_DATA \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaOutput_h_44_RPC_WRAPPERS_NO_PURE_DECLS \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaOutput_h_44_INCLASS_NO_PURE_DECLS \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaOutput_h_44_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RENDERSTREAM_API UClass* StaticClass<class URenderStreamMediaOutput>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaOutput_h


#define FOREACH_ENUM_ERENDERSTREAMALPHATYPE(op) \
	op(ERenderStreamAlphaType::NO_ACTION) \
	op(ERenderStreamAlphaType::SET_ONE) \
	op(ERenderStreamAlphaType::INVERT) 

enum class ERenderStreamAlphaType;
template<> RENDERSTREAM_API UEnum* StaticEnum<ERenderStreamAlphaType>();

#define FOREACH_ENUM_ERENDERSTREAMMEDIAOUTPUTFORMAT(op) \
	op(ERenderStreamMediaOutputFormat::BGRA) \
	op(ERenderStreamMediaOutputFormat::YUV422) \
	op(ERenderStreamMediaOutputFormat::YUV422_10b) \
	op(ERenderStreamMediaOutputFormat::YUV422_12b) \
	op(ERenderStreamMediaOutputFormat::RGB_10b) \
	op(ERenderStreamMediaOutputFormat::RGB_12b) \
	op(ERenderStreamMediaOutputFormat::RGBA_10b) \
	op(ERenderStreamMediaOutputFormat::RGBA_12b) 

enum class ERenderStreamMediaOutputFormat;
template<> RENDERSTREAM_API UEnum* StaticEnum<ERenderStreamMediaOutputFormat>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
