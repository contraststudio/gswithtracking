// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef RENDERSTREAM_RenderStreamTimecodeProvider_generated_h
#error "RenderStreamTimecodeProvider.generated.h already included, missing '#pragma once' in RenderStreamTimecodeProvider.h"
#endif
#define RENDERSTREAM_RenderStreamTimecodeProvider_generated_h

#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamTimecodeProvider_h_16_SPARSE_DATA
#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamTimecodeProvider_h_16_RPC_WRAPPERS
#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamTimecodeProvider_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamTimecodeProvider_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURenderStreamTimecodeProvider(); \
	friend struct Z_Construct_UClass_URenderStreamTimecodeProvider_Statics; \
public: \
	DECLARE_CLASS(URenderStreamTimecodeProvider, UTimecodeProvider, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RenderStream"), NO_API) \
	DECLARE_SERIALIZER(URenderStreamTimecodeProvider)


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamTimecodeProvider_h_16_INCLASS \
private: \
	static void StaticRegisterNativesURenderStreamTimecodeProvider(); \
	friend struct Z_Construct_UClass_URenderStreamTimecodeProvider_Statics; \
public: \
	DECLARE_CLASS(URenderStreamTimecodeProvider, UTimecodeProvider, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RenderStream"), NO_API) \
	DECLARE_SERIALIZER(URenderStreamTimecodeProvider)


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamTimecodeProvider_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URenderStreamTimecodeProvider(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URenderStreamTimecodeProvider) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URenderStreamTimecodeProvider); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URenderStreamTimecodeProvider); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URenderStreamTimecodeProvider(URenderStreamTimecodeProvider&&); \
	NO_API URenderStreamTimecodeProvider(const URenderStreamTimecodeProvider&); \
public:


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamTimecodeProvider_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URenderStreamTimecodeProvider(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URenderStreamTimecodeProvider(URenderStreamTimecodeProvider&&); \
	NO_API URenderStreamTimecodeProvider(const URenderStreamTimecodeProvider&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URenderStreamTimecodeProvider); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URenderStreamTimecodeProvider); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URenderStreamTimecodeProvider)


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamTimecodeProvider_h_16_PRIVATE_PROPERTY_OFFSET
#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamTimecodeProvider_h_13_PROLOG
#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamTimecodeProvider_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamTimecodeProvider_h_16_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamTimecodeProvider_h_16_SPARSE_DATA \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamTimecodeProvider_h_16_RPC_WRAPPERS \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamTimecodeProvider_h_16_INCLASS \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamTimecodeProvider_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamTimecodeProvider_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamTimecodeProvider_h_16_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamTimecodeProvider_h_16_SPARSE_DATA \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamTimecodeProvider_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamTimecodeProvider_h_16_INCLASS_NO_PURE_DECLS \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamTimecodeProvider_h_16_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class RenderStreamTimecodeProvider."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RENDERSTREAM_API UClass* StaticClass<class URenderStreamTimecodeProvider>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamTimecodeProvider_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
