// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef RENDERSTREAM_RenderStreamStatusWidget_generated_h
#error "RenderStreamStatusWidget.generated.h already included, missing '#pragma once' in RenderStreamStatusWidget.h"
#endif
#define RENDERSTREAM_RenderStreamStatusWidget_generated_h

#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamStatusWidget_h_18_SPARSE_DATA
#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamStatusWidget_h_18_RPC_WRAPPERS
#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamStatusWidget_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamStatusWidget_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURenderStreamStatusWidget(); \
	friend struct Z_Construct_UClass_URenderStreamStatusWidget_Statics; \
public: \
	DECLARE_CLASS(URenderStreamStatusWidget, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RenderStream"), NO_API) \
	DECLARE_SERIALIZER(URenderStreamStatusWidget)


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamStatusWidget_h_18_INCLASS \
private: \
	static void StaticRegisterNativesURenderStreamStatusWidget(); \
	friend struct Z_Construct_UClass_URenderStreamStatusWidget_Statics; \
public: \
	DECLARE_CLASS(URenderStreamStatusWidget, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RenderStream"), NO_API) \
	DECLARE_SERIALIZER(URenderStreamStatusWidget)


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamStatusWidget_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URenderStreamStatusWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URenderStreamStatusWidget) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URenderStreamStatusWidget); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URenderStreamStatusWidget); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URenderStreamStatusWidget(URenderStreamStatusWidget&&); \
	NO_API URenderStreamStatusWidget(const URenderStreamStatusWidget&); \
public:


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamStatusWidget_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URenderStreamStatusWidget(URenderStreamStatusWidget&&); \
	NO_API URenderStreamStatusWidget(const URenderStreamStatusWidget&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URenderStreamStatusWidget); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URenderStreamStatusWidget); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URenderStreamStatusWidget)


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamStatusWidget_h_18_PRIVATE_PROPERTY_OFFSET
#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamStatusWidget_h_14_PROLOG
#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamStatusWidget_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamStatusWidget_h_18_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamStatusWidget_h_18_SPARSE_DATA \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamStatusWidget_h_18_RPC_WRAPPERS \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamStatusWidget_h_18_INCLASS \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamStatusWidget_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamStatusWidget_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamStatusWidget_h_18_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamStatusWidget_h_18_SPARSE_DATA \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamStatusWidget_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamStatusWidget_h_18_INCLASS_NO_PURE_DECLS \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamStatusWidget_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RENDERSTREAM_API UClass* StaticClass<class URenderStreamStatusWidget>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamStatusWidget_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
