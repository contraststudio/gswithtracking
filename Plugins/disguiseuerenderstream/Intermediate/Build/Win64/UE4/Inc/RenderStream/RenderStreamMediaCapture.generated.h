// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef RENDERSTREAM_RenderStreamMediaCapture_generated_h
#error "RenderStreamMediaCapture.generated.h already included, missing '#pragma once' in RenderStreamMediaCapture.h"
#endif
#define RENDERSTREAM_RenderStreamMediaCapture_generated_h

#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaCapture_h_21_SPARSE_DATA
#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaCapture_h_21_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execUpdateSchema);


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaCapture_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execUpdateSchema);


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaCapture_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURenderStreamMediaCapture(); \
	friend struct Z_Construct_UClass_URenderStreamMediaCapture_Statics; \
public: \
	DECLARE_CLASS(URenderStreamMediaCapture, UMediaCapture, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RenderStream"), NO_API) \
	DECLARE_SERIALIZER(URenderStreamMediaCapture)


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaCapture_h_21_INCLASS \
private: \
	static void StaticRegisterNativesURenderStreamMediaCapture(); \
	friend struct Z_Construct_UClass_URenderStreamMediaCapture_Statics; \
public: \
	DECLARE_CLASS(URenderStreamMediaCapture, UMediaCapture, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RenderStream"), NO_API) \
	DECLARE_SERIALIZER(URenderStreamMediaCapture)


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaCapture_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URenderStreamMediaCapture(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URenderStreamMediaCapture) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URenderStreamMediaCapture); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URenderStreamMediaCapture); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URenderStreamMediaCapture(URenderStreamMediaCapture&&); \
	NO_API URenderStreamMediaCapture(const URenderStreamMediaCapture&); \
public:


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaCapture_h_21_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URenderStreamMediaCapture(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URenderStreamMediaCapture(URenderStreamMediaCapture&&); \
	NO_API URenderStreamMediaCapture(const URenderStreamMediaCapture&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URenderStreamMediaCapture); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URenderStreamMediaCapture); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URenderStreamMediaCapture)


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaCapture_h_21_PRIVATE_PROPERTY_OFFSET
#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaCapture_h_18_PROLOG
#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaCapture_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaCapture_h_21_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaCapture_h_21_SPARSE_DATA \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaCapture_h_21_RPC_WRAPPERS \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaCapture_h_21_INCLASS \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaCapture_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaCapture_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaCapture_h_21_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaCapture_h_21_SPARSE_DATA \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaCapture_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaCapture_h_21_INCLASS_NO_PURE_DECLS \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaCapture_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RENDERSTREAM_API UClass* StaticClass<class URenderStreamMediaCapture>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamMediaCapture_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
