// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RenderStream/Public/RenderStreamBPFunctionLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRenderStreamBPFunctionLibrary() {}
// Cross Module References
	RENDERSTREAM_API UClass* Z_Construct_UClass_URenderStreamBPFunctionLibrary_NoRegister();
	RENDERSTREAM_API UClass* Z_Construct_UClass_URenderStreamBPFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_RenderStream();
	RENDERSTREAM_API UClass* Z_Construct_UClass_URenderStreamMediaOutput_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_ACameraActor_NoRegister();
	RENDERSTREAM_API UClass* Z_Construct_UClass_URenderStreamMediaCapture_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(URenderStreamBPFunctionLibrary::execStopCapture)
	{
		P_GET_OBJECT(URenderStreamMediaCapture,Z_Param_MediaCapture);
		P_FINISH;
		P_NATIVE_BEGIN;
		URenderStreamBPFunctionLibrary::StopCapture(Z_Param_MediaCapture);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URenderStreamBPFunctionLibrary::execStartCaptureWithComponents)
	{
		P_GET_OBJECT(URenderStreamMediaOutput,Z_Param_MediaOutput);
		P_GET_OBJECT(USceneComponent,Z_Param_LocationReceiver);
		P_GET_OBJECT(USceneComponent,Z_Param_RotationReceiver);
		P_GET_OBJECT(UCameraComponent,Z_Param_CameraDataReceiver);
		P_GET_UBOOL(Z_Param_SetNewViewTarget);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(URenderStreamMediaCapture**)Z_Param__Result=URenderStreamBPFunctionLibrary::StartCaptureWithComponents(Z_Param_MediaOutput,Z_Param_LocationReceiver,Z_Param_RotationReceiver,Z_Param_CameraDataReceiver,Z_Param_SetNewViewTarget);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URenderStreamBPFunctionLibrary::execStartCapture)
	{
		P_GET_OBJECT(URenderStreamMediaOutput,Z_Param_MediaOutput);
		P_GET_OBJECT(ACameraActor,Z_Param_Camera);
		P_GET_UBOOL(Z_Param_SetNewViewTarget);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(URenderStreamMediaCapture**)Z_Param__Result=URenderStreamBPFunctionLibrary::StartCapture(Z_Param_MediaOutput,Z_Param_Camera,Z_Param_SetNewViewTarget);
		P_NATIVE_END;
	}
	void URenderStreamBPFunctionLibrary::StaticRegisterNativesURenderStreamBPFunctionLibrary()
	{
		UClass* Class = URenderStreamBPFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "StartCapture", &URenderStreamBPFunctionLibrary::execStartCapture },
			{ "StartCaptureWithComponents", &URenderStreamBPFunctionLibrary::execStartCaptureWithComponents },
			{ "StopCapture", &URenderStreamBPFunctionLibrary::execStopCapture },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCapture_Statics
	{
		struct RenderStreamBPFunctionLibrary_eventStartCapture_Parms
		{
			URenderStreamMediaOutput* MediaOutput;
			ACameraActor* Camera;
			bool SetNewViewTarget;
			URenderStreamMediaCapture* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MediaOutput;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Camera;
		static void NewProp_SetNewViewTarget_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_SetNewViewTarget;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCapture_Statics::NewProp_MediaOutput = { "MediaOutput", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RenderStreamBPFunctionLibrary_eventStartCapture_Parms, MediaOutput), Z_Construct_UClass_URenderStreamMediaOutput_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCapture_Statics::NewProp_Camera = { "Camera", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RenderStreamBPFunctionLibrary_eventStartCapture_Parms, Camera), Z_Construct_UClass_ACameraActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCapture_Statics::NewProp_SetNewViewTarget_SetBit(void* Obj)
	{
		((RenderStreamBPFunctionLibrary_eventStartCapture_Parms*)Obj)->SetNewViewTarget = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCapture_Statics::NewProp_SetNewViewTarget = { "SetNewViewTarget", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(RenderStreamBPFunctionLibrary_eventStartCapture_Parms), &Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCapture_Statics::NewProp_SetNewViewTarget_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCapture_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RenderStreamBPFunctionLibrary_eventStartCapture_Parms, ReturnValue), Z_Construct_UClass_URenderStreamMediaCapture_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCapture_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCapture_Statics::NewProp_MediaOutput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCapture_Statics::NewProp_Camera,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCapture_Statics::NewProp_SetNewViewTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCapture_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCapture_Statics::Function_MetaDataParams[] = {
		{ "Category", "DisguiseRenderStream" },
		{ "Comment", "/**  Starts Capture using the given Camera for tracking and view if desired\n    *\n    * @param MediaOutput - RenderStreamMediaOutput asset to use\n    * @param Camera - CameraActor or subclass of CameraActor to use for view and tracking\n    * @param SetNewViewTarget - true if the CameraActor provided will be the new main view target, otherwise captue will use whatever the current main view is\n    * @return RenderStreamMediaCapture created or none on failure\n    */" },
		{ "CPP_Default_SetNewViewTarget", "true" },
		{ "ModuleRelativePath", "Public/RenderStreamBPFunctionLibrary.h" },
		{ "ToolTip", "Starts Capture using the given Camera for tracking and view if desired\n\n@param MediaOutput - RenderStreamMediaOutput asset to use\n@param Camera - CameraActor or subclass of CameraActor to use for view and tracking\n@param SetNewViewTarget - true if the CameraActor provided will be the new main view target, otherwise captue will use whatever the current main view is\n@return RenderStreamMediaCapture created or none on failure" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCapture_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URenderStreamBPFunctionLibrary, nullptr, "StartCapture", nullptr, nullptr, sizeof(RenderStreamBPFunctionLibrary_eventStartCapture_Parms), Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCapture_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCapture_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCapture_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCapture_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCapture()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCapture_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents_Statics
	{
		struct RenderStreamBPFunctionLibrary_eventStartCaptureWithComponents_Parms
		{
			URenderStreamMediaOutput* MediaOutput;
			USceneComponent* LocationReceiver;
			USceneComponent* RotationReceiver;
			UCameraComponent* CameraDataReceiver;
			bool SetNewViewTarget;
			URenderStreamMediaCapture* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MediaOutput;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LocationReceiver_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LocationReceiver;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RotationReceiver_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RotationReceiver;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraDataReceiver_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CameraDataReceiver;
		static void NewProp_SetNewViewTarget_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_SetNewViewTarget;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents_Statics::NewProp_MediaOutput = { "MediaOutput", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RenderStreamBPFunctionLibrary_eventStartCaptureWithComponents_Parms, MediaOutput), Z_Construct_UClass_URenderStreamMediaOutput_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents_Statics::NewProp_LocationReceiver_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents_Statics::NewProp_LocationReceiver = { "LocationReceiver", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RenderStreamBPFunctionLibrary_eventStartCaptureWithComponents_Parms, LocationReceiver), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents_Statics::NewProp_LocationReceiver_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents_Statics::NewProp_LocationReceiver_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents_Statics::NewProp_RotationReceiver_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents_Statics::NewProp_RotationReceiver = { "RotationReceiver", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RenderStreamBPFunctionLibrary_eventStartCaptureWithComponents_Parms, RotationReceiver), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents_Statics::NewProp_RotationReceiver_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents_Statics::NewProp_RotationReceiver_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents_Statics::NewProp_CameraDataReceiver_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents_Statics::NewProp_CameraDataReceiver = { "CameraDataReceiver", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RenderStreamBPFunctionLibrary_eventStartCaptureWithComponents_Parms, CameraDataReceiver), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents_Statics::NewProp_CameraDataReceiver_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents_Statics::NewProp_CameraDataReceiver_MetaData)) };
	void Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents_Statics::NewProp_SetNewViewTarget_SetBit(void* Obj)
	{
		((RenderStreamBPFunctionLibrary_eventStartCaptureWithComponents_Parms*)Obj)->SetNewViewTarget = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents_Statics::NewProp_SetNewViewTarget = { "SetNewViewTarget", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(RenderStreamBPFunctionLibrary_eventStartCaptureWithComponents_Parms), &Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents_Statics::NewProp_SetNewViewTarget_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RenderStreamBPFunctionLibrary_eventStartCaptureWithComponents_Parms, ReturnValue), Z_Construct_UClass_URenderStreamMediaCapture_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents_Statics::NewProp_MediaOutput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents_Statics::NewProp_LocationReceiver,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents_Statics::NewProp_RotationReceiver,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents_Statics::NewProp_CameraDataReceiver,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents_Statics::NewProp_SetNewViewTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents_Statics::Function_MetaDataParams[] = {
		{ "Category", "DisguiseRenderStream" },
		{ "Comment", "/**  Starts Capture using the given Components for tracking and the actor of the camera component for view if desired\n    *\n    * Main viewport will be set to the viewport of the given camera\n    * @param MediaOutput - RenderStreamMediaOutput asset to use\n    * @param LocationReceiver - SceneComponent to receive tracking position data, none means no position tracking\n    * @param RotationReceiver - SceneCOmponent to receive tracking rotational data, none means no rotation tracking\n    * @param CameraDataReceiver - CameraComponent or subclass of to use for view and receive camera data, the atachment actor of this component is the target of the view\n    * @param SetNewViewTarget - true if the Actor that the CameraComponent provided is attached to will be the new main view target, otherwise captue will use whatever the current main view is\n    * @return RenderStreamMediaCapture created or none on failure\n    */" },
		{ "CPP_Default_SetNewViewTarget", "true" },
		{ "ModuleRelativePath", "Public/RenderStreamBPFunctionLibrary.h" },
		{ "ToolTip", "Starts Capture using the given Components for tracking and the actor of the camera component for view if desired\n\nMain viewport will be set to the viewport of the given camera\n@param MediaOutput - RenderStreamMediaOutput asset to use\n@param LocationReceiver - SceneComponent to receive tracking position data, none means no position tracking\n@param RotationReceiver - SceneCOmponent to receive tracking rotational data, none means no rotation tracking\n@param CameraDataReceiver - CameraComponent or subclass of to use for view and receive camera data, the atachment actor of this component is the target of the view\n@param SetNewViewTarget - true if the Actor that the CameraComponent provided is attached to will be the new main view target, otherwise captue will use whatever the current main view is\n@return RenderStreamMediaCapture created or none on failure" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URenderStreamBPFunctionLibrary, nullptr, "StartCaptureWithComponents", nullptr, nullptr, sizeof(RenderStreamBPFunctionLibrary_eventStartCaptureWithComponents_Parms), Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StopCapture_Statics
	{
		struct RenderStreamBPFunctionLibrary_eventStopCapture_Parms
		{
			URenderStreamMediaCapture* MediaCapture;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MediaCapture;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StopCapture_Statics::NewProp_MediaCapture = { "MediaCapture", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RenderStreamBPFunctionLibrary_eventStopCapture_Parms, MediaCapture), Z_Construct_UClass_URenderStreamMediaCapture_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StopCapture_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StopCapture_Statics::NewProp_MediaCapture,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StopCapture_Statics::Function_MetaDataParams[] = {
		{ "Category", "DisguiseRenderStream" },
		{ "Comment", "/**  Stop Capturing with the given RenderStreamMediaCapture\n    *\n    * Ensures any pending frames are processed before stopping\n    * @param MediaCapture - RenderStreamMediaCapture object that is currently capturing\n    */" },
		{ "ModuleRelativePath", "Public/RenderStreamBPFunctionLibrary.h" },
		{ "ToolTip", "Stop Capturing with the given RenderStreamMediaCapture\n\nEnsures any pending frames are processed before stopping\n@param MediaCapture - RenderStreamMediaCapture object that is currently capturing" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StopCapture_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URenderStreamBPFunctionLibrary, nullptr, "StopCapture", nullptr, nullptr, sizeof(RenderStreamBPFunctionLibrary_eventStopCapture_Parms), Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StopCapture_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StopCapture_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StopCapture_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StopCapture_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StopCapture()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StopCapture_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_URenderStreamBPFunctionLibrary_NoRegister()
	{
		return URenderStreamBPFunctionLibrary::StaticClass();
	}
	struct Z_Construct_UClass_URenderStreamBPFunctionLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URenderStreamBPFunctionLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_RenderStream,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_URenderStreamBPFunctionLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCapture, "StartCapture" }, // 3212025229
		{ &Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StartCaptureWithComponents, "StartCaptureWithComponents" }, // 854126725
		{ &Z_Construct_UFunction_URenderStreamBPFunctionLibrary_StopCapture, "StopCapture" }, // 509911338
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URenderStreamBPFunctionLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "RenderStreamBPFunctionLibrary.h" },
		{ "ModuleRelativePath", "Public/RenderStreamBPFunctionLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_URenderStreamBPFunctionLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URenderStreamBPFunctionLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URenderStreamBPFunctionLibrary_Statics::ClassParams = {
		&URenderStreamBPFunctionLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_URenderStreamBPFunctionLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URenderStreamBPFunctionLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URenderStreamBPFunctionLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URenderStreamBPFunctionLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URenderStreamBPFunctionLibrary, 2920667400);
	template<> RENDERSTREAM_API UClass* StaticClass<URenderStreamBPFunctionLibrary>()
	{
		return URenderStreamBPFunctionLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URenderStreamBPFunctionLibrary(Z_Construct_UClass_URenderStreamBPFunctionLibrary, &URenderStreamBPFunctionLibrary::StaticClass, TEXT("/Script/RenderStream"), TEXT("URenderStreamBPFunctionLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URenderStreamBPFunctionLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
