// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RenderStreamEditor/Public/RenderStreamMediaOutputFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRenderStreamMediaOutputFactory() {}
// Cross Module References
	RENDERSTREAMEDITOR_API UClass* Z_Construct_UClass_URenderStreamMediaOutputFactory_NoRegister();
	RENDERSTREAMEDITOR_API UClass* Z_Construct_UClass_URenderStreamMediaOutputFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_RenderStreamEditor();
// End Cross Module References
	void URenderStreamMediaOutputFactory::StaticRegisterNativesURenderStreamMediaOutputFactory()
	{
	}
	UClass* Z_Construct_UClass_URenderStreamMediaOutputFactory_NoRegister()
	{
		return URenderStreamMediaOutputFactory::StaticClass();
	}
	struct Z_Construct_UClass_URenderStreamMediaOutputFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URenderStreamMediaOutputFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_RenderStreamEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URenderStreamMediaOutputFactory_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "RenderStreamMediaOutputFactory.h" },
		{ "ModuleRelativePath", "Public/RenderStreamMediaOutputFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_URenderStreamMediaOutputFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URenderStreamMediaOutputFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URenderStreamMediaOutputFactory_Statics::ClassParams = {
		&URenderStreamMediaOutputFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_URenderStreamMediaOutputFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URenderStreamMediaOutputFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URenderStreamMediaOutputFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URenderStreamMediaOutputFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URenderStreamMediaOutputFactory, 1750143282);
	template<> RENDERSTREAMEDITOR_API UClass* StaticClass<URenderStreamMediaOutputFactory>()
	{
		return URenderStreamMediaOutputFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URenderStreamMediaOutputFactory(Z_Construct_UClass_URenderStreamMediaOutputFactory, &URenderStreamMediaOutputFactory::StaticClass, TEXT("/Script/RenderStreamEditor"), TEXT("URenderStreamMediaOutputFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URenderStreamMediaOutputFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
