// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef RENDERSTREAMEDITOR_RenderStreamMediaOutputFactory_generated_h
#error "RenderStreamMediaOutputFactory.generated.h already included, missing '#pragma once' in RenderStreamMediaOutputFactory.h"
#endif
#define RENDERSTREAMEDITOR_RenderStreamMediaOutputFactory_generated_h

#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStreamEditor_Public_RenderStreamMediaOutputFactory_h_15_SPARSE_DATA
#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStreamEditor_Public_RenderStreamMediaOutputFactory_h_15_RPC_WRAPPERS
#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStreamEditor_Public_RenderStreamMediaOutputFactory_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStreamEditor_Public_RenderStreamMediaOutputFactory_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURenderStreamMediaOutputFactory(); \
	friend struct Z_Construct_UClass_URenderStreamMediaOutputFactory_Statics; \
public: \
	DECLARE_CLASS(URenderStreamMediaOutputFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RenderStreamEditor"), NO_API) \
	DECLARE_SERIALIZER(URenderStreamMediaOutputFactory)


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStreamEditor_Public_RenderStreamMediaOutputFactory_h_15_INCLASS \
private: \
	static void StaticRegisterNativesURenderStreamMediaOutputFactory(); \
	friend struct Z_Construct_UClass_URenderStreamMediaOutputFactory_Statics; \
public: \
	DECLARE_CLASS(URenderStreamMediaOutputFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RenderStreamEditor"), NO_API) \
	DECLARE_SERIALIZER(URenderStreamMediaOutputFactory)


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStreamEditor_Public_RenderStreamMediaOutputFactory_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URenderStreamMediaOutputFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URenderStreamMediaOutputFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URenderStreamMediaOutputFactory); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URenderStreamMediaOutputFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URenderStreamMediaOutputFactory(URenderStreamMediaOutputFactory&&); \
	NO_API URenderStreamMediaOutputFactory(const URenderStreamMediaOutputFactory&); \
public:


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStreamEditor_Public_RenderStreamMediaOutputFactory_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URenderStreamMediaOutputFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URenderStreamMediaOutputFactory(URenderStreamMediaOutputFactory&&); \
	NO_API URenderStreamMediaOutputFactory(const URenderStreamMediaOutputFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URenderStreamMediaOutputFactory); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URenderStreamMediaOutputFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URenderStreamMediaOutputFactory)


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStreamEditor_Public_RenderStreamMediaOutputFactory_h_15_PRIVATE_PROPERTY_OFFSET
#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStreamEditor_Public_RenderStreamMediaOutputFactory_h_12_PROLOG
#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStreamEditor_Public_RenderStreamMediaOutputFactory_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStreamEditor_Public_RenderStreamMediaOutputFactory_h_15_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStreamEditor_Public_RenderStreamMediaOutputFactory_h_15_SPARSE_DATA \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStreamEditor_Public_RenderStreamMediaOutputFactory_h_15_RPC_WRAPPERS \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStreamEditor_Public_RenderStreamMediaOutputFactory_h_15_INCLASS \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStreamEditor_Public_RenderStreamMediaOutputFactory_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStreamEditor_Public_RenderStreamMediaOutputFactory_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStreamEditor_Public_RenderStreamMediaOutputFactory_h_15_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStreamEditor_Public_RenderStreamMediaOutputFactory_h_15_SPARSE_DATA \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStreamEditor_Public_RenderStreamMediaOutputFactory_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStreamEditor_Public_RenderStreamMediaOutputFactory_h_15_INCLASS_NO_PURE_DECLS \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStreamEditor_Public_RenderStreamMediaOutputFactory_h_15_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class RenderStreamMediaOutputFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RENDERSTREAMEDITOR_API UClass* StaticClass<class URenderStreamMediaOutputFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID HostProject_Plugins_disguiseuerenderstream_Source_RenderStreamEditor_Public_RenderStreamMediaOutputFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
