// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class URenderStreamMediaCapture;
class URenderStreamMediaOutput;
class USceneComponent;
class UCameraComponent;
class ACameraActor;
#ifdef RENDERSTREAM_RenderStreamBPFunctionLibrary_generated_h
#error "RenderStreamBPFunctionLibrary.generated.h already included, missing '#pragma once' in RenderStreamBPFunctionLibrary.h"
#endif
#define RENDERSTREAM_RenderStreamBPFunctionLibrary_generated_h

#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamBPFunctionLibrary_h_17_SPARSE_DATA
#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamBPFunctionLibrary_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execStopCapture); \
	DECLARE_FUNCTION(execStartCaptureWithComponents); \
	DECLARE_FUNCTION(execStartCapture);


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamBPFunctionLibrary_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execStopCapture); \
	DECLARE_FUNCTION(execStartCaptureWithComponents); \
	DECLARE_FUNCTION(execStartCapture);


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamBPFunctionLibrary_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURenderStreamBPFunctionLibrary(); \
	friend struct Z_Construct_UClass_URenderStreamBPFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(URenderStreamBPFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RenderStream"), NO_API) \
	DECLARE_SERIALIZER(URenderStreamBPFunctionLibrary)


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamBPFunctionLibrary_h_17_INCLASS \
private: \
	static void StaticRegisterNativesURenderStreamBPFunctionLibrary(); \
	friend struct Z_Construct_UClass_URenderStreamBPFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(URenderStreamBPFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RenderStream"), NO_API) \
	DECLARE_SERIALIZER(URenderStreamBPFunctionLibrary)


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamBPFunctionLibrary_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URenderStreamBPFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URenderStreamBPFunctionLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URenderStreamBPFunctionLibrary); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URenderStreamBPFunctionLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URenderStreamBPFunctionLibrary(URenderStreamBPFunctionLibrary&&); \
	NO_API URenderStreamBPFunctionLibrary(const URenderStreamBPFunctionLibrary&); \
public:


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamBPFunctionLibrary_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URenderStreamBPFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URenderStreamBPFunctionLibrary(URenderStreamBPFunctionLibrary&&); \
	NO_API URenderStreamBPFunctionLibrary(const URenderStreamBPFunctionLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URenderStreamBPFunctionLibrary); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URenderStreamBPFunctionLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URenderStreamBPFunctionLibrary)


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamBPFunctionLibrary_h_17_PRIVATE_PROPERTY_OFFSET
#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamBPFunctionLibrary_h_14_PROLOG
#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamBPFunctionLibrary_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamBPFunctionLibrary_h_17_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamBPFunctionLibrary_h_17_SPARSE_DATA \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamBPFunctionLibrary_h_17_RPC_WRAPPERS \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamBPFunctionLibrary_h_17_INCLASS \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamBPFunctionLibrary_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamBPFunctionLibrary_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamBPFunctionLibrary_h_17_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamBPFunctionLibrary_h_17_SPARSE_DATA \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamBPFunctionLibrary_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamBPFunctionLibrary_h_17_INCLASS_NO_PURE_DECLS \
	HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamBPFunctionLibrary_h_17_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class RenderStreamBPFunctionLibrary."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RENDERSTREAM_API UClass* StaticClass<class URenderStreamBPFunctionLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID HostProject_Plugins_disguiseuerenderstream_Source_RenderStream_Public_RenderStreamBPFunctionLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
