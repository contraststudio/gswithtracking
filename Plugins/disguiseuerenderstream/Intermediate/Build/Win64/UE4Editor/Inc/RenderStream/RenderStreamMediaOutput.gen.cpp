// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RenderStream/Public/RenderStreamMediaOutput.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRenderStreamMediaOutput() {}
// Cross Module References
	RENDERSTREAM_API UEnum* Z_Construct_UEnum_RenderStream_ERenderStreamAlphaType();
	UPackage* Z_Construct_UPackage__Script_RenderStream();
	RENDERSTREAM_API UEnum* Z_Construct_UEnum_RenderStream_ERenderStreamMediaOutputFormat();
	RENDERSTREAM_API UClass* Z_Construct_UClass_URenderStreamMediaOutput_NoRegister();
	RENDERSTREAM_API UClass* Z_Construct_UClass_URenderStreamMediaOutput();
	MEDIAIOCORE_API UClass* Z_Construct_UClass_UMediaOutput();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FIntPoint();
	RENDERSTREAM_API UClass* Z_Construct_UClass_URenderStreamTimecodeProvider_NoRegister();
// End Cross Module References
	static UEnum* ERenderStreamAlphaType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_RenderStream_ERenderStreamAlphaType, Z_Construct_UPackage__Script_RenderStream(), TEXT("ERenderStreamAlphaType"));
		}
		return Singleton;
	}
	template<> RENDERSTREAM_API UEnum* StaticEnum<ERenderStreamAlphaType>()
	{
		return ERenderStreamAlphaType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ERenderStreamAlphaType(ERenderStreamAlphaType_StaticEnum, TEXT("/Script/RenderStream"), TEXT("ERenderStreamAlphaType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_RenderStream_ERenderStreamAlphaType_Hash() { return 2443580457U; }
	UEnum* Z_Construct_UEnum_RenderStream_ERenderStreamAlphaType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_RenderStream();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ERenderStreamAlphaType"), 0, Get_Z_Construct_UEnum_RenderStream_ERenderStreamAlphaType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ERenderStreamAlphaType::NO_ACTION", (int64)ERenderStreamAlphaType::NO_ACTION },
				{ "ERenderStreamAlphaType::SET_ONE", (int64)ERenderStreamAlphaType::SET_ONE },
				{ "ERenderStreamAlphaType::INVERT", (int64)ERenderStreamAlphaType::INVERT },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "INVERT.DisplayName", "Invert Alpha" },
				{ "INVERT.Name", "ERenderStreamAlphaType::INVERT" },
				{ "ModuleRelativePath", "Public/RenderStreamMediaOutput.h" },
				{ "NO_ACTION.Display", "No Action" },
				{ "NO_ACTION.Name", "ERenderStreamAlphaType::NO_ACTION" },
				{ "SET_ONE.DisplayName", "Set to 1" },
				{ "SET_ONE.Name", "ERenderStreamAlphaType::SET_ONE" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_RenderStream,
				nullptr,
				"ERenderStreamAlphaType",
				"ERenderStreamAlphaType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ERenderStreamMediaOutputFormat_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_RenderStream_ERenderStreamMediaOutputFormat, Z_Construct_UPackage__Script_RenderStream(), TEXT("ERenderStreamMediaOutputFormat"));
		}
		return Singleton;
	}
	template<> RENDERSTREAM_API UEnum* StaticEnum<ERenderStreamMediaOutputFormat>()
	{
		return ERenderStreamMediaOutputFormat_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ERenderStreamMediaOutputFormat(ERenderStreamMediaOutputFormat_StaticEnum, TEXT("/Script/RenderStream"), TEXT("ERenderStreamMediaOutputFormat"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_RenderStream_ERenderStreamMediaOutputFormat_Hash() { return 134753881U; }
	UEnum* Z_Construct_UEnum_RenderStream_ERenderStreamMediaOutputFormat()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_RenderStream();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ERenderStreamMediaOutputFormat"), 0, Get_Z_Construct_UEnum_RenderStream_ERenderStreamMediaOutputFormat_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ERenderStreamMediaOutputFormat::BGRA", (int64)ERenderStreamMediaOutputFormat::BGRA },
				{ "ERenderStreamMediaOutputFormat::YUV422", (int64)ERenderStreamMediaOutputFormat::YUV422 },
				{ "ERenderStreamMediaOutputFormat::YUV422_10b", (int64)ERenderStreamMediaOutputFormat::YUV422_10b },
				{ "ERenderStreamMediaOutputFormat::YUV422_12b", (int64)ERenderStreamMediaOutputFormat::YUV422_12b },
				{ "ERenderStreamMediaOutputFormat::RGB_10b", (int64)ERenderStreamMediaOutputFormat::RGB_10b },
				{ "ERenderStreamMediaOutputFormat::RGB_12b", (int64)ERenderStreamMediaOutputFormat::RGB_12b },
				{ "ERenderStreamMediaOutputFormat::RGBA_10b", (int64)ERenderStreamMediaOutputFormat::RGBA_10b },
				{ "ERenderStreamMediaOutputFormat::RGBA_12b", (int64)ERenderStreamMediaOutputFormat::RGBA_12b },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BGRA.Comment", "//RGBA\x09UMETA(DisplayName = \"RGBA 8bit\"),\n" },
				{ "BGRA.DisplayName", "BGRA 8bit (NDI)" },
				{ "BGRA.Name", "ERenderStreamMediaOutputFormat::BGRA" },
				{ "BGRA.ToolTip", "RGBA    UMETA(DisplayName = \"RGBA 8bit\")," },
				{ "ModuleRelativePath", "Public/RenderStreamMediaOutput.h" },
				{ "RGB_10b.DisplayName", "RGB 10bit (UC)" },
				{ "RGB_10b.Name", "ERenderStreamMediaOutputFormat::RGB_10b" },
				{ "RGB_12b.DisplayName", "RGB 12bit (UC)" },
				{ "RGB_12b.Name", "ERenderStreamMediaOutputFormat::RGB_12b" },
				{ "RGBA_10b.DisplayName", "RGBA 10bit (UC)" },
				{ "RGBA_10b.Name", "ERenderStreamMediaOutputFormat::RGBA_10b" },
				{ "RGBA_12b.DisplayName", "RGBA 12bit (UC)" },
				{ "RGBA_12b.Name", "ERenderStreamMediaOutputFormat::RGBA_12b" },
				{ "YUV422.DisplayName", "YUV 4:2:2 8bit (NDI)" },
				{ "YUV422.Name", "ERenderStreamMediaOutputFormat::YUV422" },
				{ "YUV422_10b.DisplayName", "YUV 4:2:2 10bit (UC)" },
				{ "YUV422_10b.Name", "ERenderStreamMediaOutputFormat::YUV422_10b" },
				{ "YUV422_12b.DisplayName", "YUV 4:2:2 12bit (UC)" },
				{ "YUV422_12b.Name", "ERenderStreamMediaOutputFormat::YUV422_12b" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_RenderStream,
				nullptr,
				"ERenderStreamMediaOutputFormat",
				"ERenderStreamMediaOutputFormat",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void URenderStreamMediaOutput::StaticRegisterNativesURenderStreamMediaOutput()
	{
	}
	UClass* Z_Construct_UClass_URenderStreamMediaOutput_NoRegister()
	{
		return URenderStreamMediaOutput::StaticClass();
	}
	struct Z_Construct_UClass_URenderStreamMediaOutput_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_m_overrideSize_MetaData[];
#endif
		static void NewProp_m_overrideSize_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_m_overrideSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_m_desiredSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_m_desiredSize;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_m_outputFormat_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_m_outputFormat_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_m_outputFormat;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_m_timecode_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_m_timecode;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_m_alphatype_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_m_alphatype_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_m_alphatype;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_m_framerateNumerator_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_m_framerateNumerator;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_m_framerateDenominator_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_m_framerateDenominator;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_m_opencl_MetaData[];
#endif
		static void NewProp_m_opencl_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_m_opencl;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URenderStreamMediaOutput_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMediaOutput,
		(UObject* (*)())Z_Construct_UPackage__Script_RenderStream,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URenderStreamMediaOutput_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "Disguise" },
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "RenderStreamMediaOutput.h" },
		{ "ModuleRelativePath", "Public/RenderStreamMediaOutput.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_overrideSize_MetaData[] = {
		{ "Category", "DisguiseRenderStream" },
		{ "DisplayName", "Override Size" },
		{ "ModuleRelativePath", "Public/RenderStreamMediaOutput.h" },
	};
#endif
	void Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_overrideSize_SetBit(void* Obj)
	{
		((URenderStreamMediaOutput*)Obj)->m_overrideSize = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_overrideSize = { "m_overrideSize", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URenderStreamMediaOutput), &Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_overrideSize_SetBit, METADATA_PARAMS(Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_overrideSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_overrideSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_desiredSize_MetaData[] = {
		{ "Category", "DisguiseRenderStream" },
		{ "DisplayName", "Desired Size" },
		{ "EditCondition", "m_overrideSize" },
		{ "ModuleRelativePath", "Public/RenderStreamMediaOutput.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_desiredSize = { "m_desiredSize", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URenderStreamMediaOutput, m_desiredSize), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_desiredSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_desiredSize_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_outputFormat_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_outputFormat_MetaData[] = {
		{ "Category", "DisguiseRenderStream" },
		{ "DisplayName", "Output Format" },
		{ "ModuleRelativePath", "Public/RenderStreamMediaOutput.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_outputFormat = { "m_outputFormat", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URenderStreamMediaOutput, m_outputFormat), Z_Construct_UEnum_RenderStream_ERenderStreamMediaOutputFormat, METADATA_PARAMS(Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_outputFormat_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_outputFormat_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_timecode_MetaData[] = {
		{ "Category", "Timecode" },
		{ "Comment", "// If set, when receiving the stream, this object is populated with the timecode distributed from disguise\n" },
		{ "DisplayName", "Associated Timecode" },
		{ "ModuleRelativePath", "Public/RenderStreamMediaOutput.h" },
		{ "ToolTip", "If set, when receiving the stream, this object is populated with the timecode distributed from disguise" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_timecode = { "m_timecode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URenderStreamMediaOutput, m_timecode), Z_Construct_UClass_URenderStreamTimecodeProvider_NoRegister, METADATA_PARAMS(Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_timecode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_timecode_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_alphatype_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_alphatype_MetaData[] = {
		{ "Category", "DisguiseRenderStream" },
		{ "Comment", "// Sets the operation performed on alpha channel when alpha is provided\n" },
		{ "DisplayName", "Alpha Action" },
		{ "ModuleRelativePath", "Public/RenderStreamMediaOutput.h" },
		{ "ToolTip", "Sets the operation performed on alpha channel when alpha is provided" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_alphatype = { "m_alphatype", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URenderStreamMediaOutput, m_alphatype), Z_Construct_UEnum_RenderStream_ERenderStreamAlphaType, METADATA_PARAMS(Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_alphatype_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_alphatype_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_framerateNumerator_MetaData[] = {
		{ "Category", "RenderStream Uncompressed" },
		{ "DisplayName", "UC Framerate (numerator)" },
		{ "ModuleRelativePath", "Public/RenderStreamMediaOutput.h" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_framerateNumerator = { "m_framerateNumerator", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URenderStreamMediaOutput, m_framerateNumerator), METADATA_PARAMS(Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_framerateNumerator_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_framerateNumerator_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_framerateDenominator_MetaData[] = {
		{ "Category", "RenderStream Uncompressed" },
		{ "DisplayName", "UC Framerate (denominator)" },
		{ "ModuleRelativePath", "Public/RenderStreamMediaOutput.h" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_framerateDenominator = { "m_framerateDenominator", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URenderStreamMediaOutput, m_framerateDenominator), METADATA_PARAMS(Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_framerateDenominator_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_framerateDenominator_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_opencl_MetaData[] = {
		{ "Category", "RenderStream Uncompressed" },
		{ "DisplayName", "UC Use OpenCL" },
		{ "ModuleRelativePath", "Public/RenderStreamMediaOutput.h" },
	};
#endif
	void Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_opencl_SetBit(void* Obj)
	{
		((URenderStreamMediaOutput*)Obj)->m_opencl = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_opencl = { "m_opencl", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URenderStreamMediaOutput), &Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_opencl_SetBit, METADATA_PARAMS(Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_opencl_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_opencl_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URenderStreamMediaOutput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_overrideSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_desiredSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_outputFormat_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_outputFormat,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_timecode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_alphatype_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_alphatype,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_framerateNumerator,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_framerateDenominator,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URenderStreamMediaOutput_Statics::NewProp_m_opencl,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URenderStreamMediaOutput_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URenderStreamMediaOutput>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URenderStreamMediaOutput_Statics::ClassParams = {
		&URenderStreamMediaOutput::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URenderStreamMediaOutput_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URenderStreamMediaOutput_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_URenderStreamMediaOutput_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URenderStreamMediaOutput_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URenderStreamMediaOutput()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URenderStreamMediaOutput_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URenderStreamMediaOutput, 3904268317);
	template<> RENDERSTREAM_API UClass* StaticClass<URenderStreamMediaOutput>()
	{
		return URenderStreamMediaOutput::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URenderStreamMediaOutput(Z_Construct_UClass_URenderStreamMediaOutput, &URenderStreamMediaOutput::StaticClass, TEXT("/Script/RenderStream"), TEXT("URenderStreamMediaOutput"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URenderStreamMediaOutput);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
