// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RenderStream/Public/RenderStreamMediaCapture.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRenderStreamMediaCapture() {}
// Cross Module References
	RENDERSTREAM_API UClass* Z_Construct_UClass_URenderStreamMediaCapture_NoRegister();
	RENDERSTREAM_API UClass* Z_Construct_UClass_URenderStreamMediaCapture();
	MEDIAIOCORE_API UClass* Z_Construct_UClass_UMediaCapture();
	UPackage* Z_Construct_UPackage__Script_RenderStream();
// End Cross Module References
	DEFINE_FUNCTION(URenderStreamMediaCapture::execUpdateSchema)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->UpdateSchema();
		P_NATIVE_END;
	}
	void URenderStreamMediaCapture::StaticRegisterNativesURenderStreamMediaCapture()
	{
		UClass* Class = URenderStreamMediaCapture::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "UpdateSchema", &URenderStreamMediaCapture::execUpdateSchema },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_URenderStreamMediaCapture_UpdateSchema_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URenderStreamMediaCapture_UpdateSchema_Statics::Function_MetaDataParams[] = {
		{ "Category", "Callback" },
		{ "ModuleRelativePath", "Public/RenderStreamMediaCapture.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_URenderStreamMediaCapture_UpdateSchema_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URenderStreamMediaCapture, nullptr, "UpdateSchema", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x44020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URenderStreamMediaCapture_UpdateSchema_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URenderStreamMediaCapture_UpdateSchema_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URenderStreamMediaCapture_UpdateSchema()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_URenderStreamMediaCapture_UpdateSchema_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_URenderStreamMediaCapture_NoRegister()
	{
		return URenderStreamMediaCapture::StaticClass();
	}
	struct Z_Construct_UClass_URenderStreamMediaCapture_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URenderStreamMediaCapture_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMediaCapture,
		(UObject* (*)())Z_Construct_UPackage__Script_RenderStream,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_URenderStreamMediaCapture_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_URenderStreamMediaCapture_UpdateSchema, "UpdateSchema" }, // 1373516204
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URenderStreamMediaCapture_Statics::Class_MetaDataParams[] = {
		{ "ClassGroupNames", "Disguise" },
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "RenderStreamMediaCapture.h" },
		{ "ModuleRelativePath", "Public/RenderStreamMediaCapture.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_URenderStreamMediaCapture_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URenderStreamMediaCapture>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URenderStreamMediaCapture_Statics::ClassParams = {
		&URenderStreamMediaCapture::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x009010A0u,
		METADATA_PARAMS(Z_Construct_UClass_URenderStreamMediaCapture_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URenderStreamMediaCapture_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URenderStreamMediaCapture()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URenderStreamMediaCapture_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URenderStreamMediaCapture, 2961626646);
	template<> RENDERSTREAM_API UClass* StaticClass<URenderStreamMediaCapture>()
	{
		return URenderStreamMediaCapture::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URenderStreamMediaCapture(Z_Construct_UClass_URenderStreamMediaCapture, &URenderStreamMediaCapture::StaticClass, TEXT("/Script/RenderStream"), TEXT("URenderStreamMediaCapture"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URenderStreamMediaCapture);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
