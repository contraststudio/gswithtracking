// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RenderStream/Public/RenderStreamSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRenderStreamSettings() {}
// Cross Module References
	RENDERSTREAM_API UClass* Z_Construct_UClass_URenderStreamSettings_NoRegister();
	RENDERSTREAM_API UClass* Z_Construct_UClass_URenderStreamSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_RenderStream();
// End Cross Module References
	void URenderStreamSettings::StaticRegisterNativesURenderStreamSettings()
	{
	}
	UClass* Z_Construct_UClass_URenderStreamSettings_NoRegister()
	{
		return URenderStreamSettings::StaticClass();
	}
	struct Z_Construct_UClass_URenderStreamSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bGenerateScenesFromLevels_MetaData[];
#endif
		static void NewProp_bGenerateScenesFromLevels_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bGenerateScenesFromLevels;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URenderStreamSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_RenderStream,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URenderStreamSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n* Implements the settings for the RenderStream plugin.\n*/" },
		{ "IncludePath", "RenderStreamSettings.h" },
		{ "ModuleRelativePath", "Public/RenderStreamSettings.h" },
		{ "ToolTip", "Implements the settings for the RenderStream plugin." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URenderStreamSettings_Statics::NewProp_bGenerateScenesFromLevels_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/RenderStreamSettings.h" },
	};
#endif
	void Z_Construct_UClass_URenderStreamSettings_Statics::NewProp_bGenerateScenesFromLevels_SetBit(void* Obj)
	{
		((URenderStreamSettings*)Obj)->bGenerateScenesFromLevels = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URenderStreamSettings_Statics::NewProp_bGenerateScenesFromLevels = { "bGenerateScenesFromLevels", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URenderStreamSettings), &Z_Construct_UClass_URenderStreamSettings_Statics::NewProp_bGenerateScenesFromLevels_SetBit, METADATA_PARAMS(Z_Construct_UClass_URenderStreamSettings_Statics::NewProp_bGenerateScenesFromLevels_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URenderStreamSettings_Statics::NewProp_bGenerateScenesFromLevels_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URenderStreamSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URenderStreamSettings_Statics::NewProp_bGenerateScenesFromLevels,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URenderStreamSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URenderStreamSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URenderStreamSettings_Statics::ClassParams = {
		&URenderStreamSettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URenderStreamSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URenderStreamSettings_Statics::PropPointers),
		0,
		0x001000A6u,
		METADATA_PARAMS(Z_Construct_UClass_URenderStreamSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URenderStreamSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URenderStreamSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URenderStreamSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URenderStreamSettings, 4136670806);
	template<> RENDERSTREAM_API UClass* StaticClass<URenderStreamSettings>()
	{
		return URenderStreamSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URenderStreamSettings(Z_Construct_UClass_URenderStreamSettings, &URenderStreamSettings::StaticClass, TEXT("/Script/RenderStream"), TEXT("URenderStreamSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URenderStreamSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
