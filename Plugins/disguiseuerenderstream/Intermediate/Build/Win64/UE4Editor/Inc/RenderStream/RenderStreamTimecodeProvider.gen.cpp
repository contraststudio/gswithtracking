// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RenderStream/Public/RenderStreamTimecodeProvider.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRenderStreamTimecodeProvider() {}
// Cross Module References
	RENDERSTREAM_API UClass* Z_Construct_UClass_URenderStreamTimecodeProvider_NoRegister();
	RENDERSTREAM_API UClass* Z_Construct_UClass_URenderStreamTimecodeProvider();
	ENGINE_API UClass* Z_Construct_UClass_UTimecodeProvider();
	UPackage* Z_Construct_UPackage__Script_RenderStream();
// End Cross Module References
	void URenderStreamTimecodeProvider::StaticRegisterNativesURenderStreamTimecodeProvider()
	{
	}
	UClass* Z_Construct_UClass_URenderStreamTimecodeProvider_NoRegister()
	{
		return URenderStreamTimecodeProvider::StaticClass();
	}
	struct Z_Construct_UClass_URenderStreamTimecodeProvider_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URenderStreamTimecodeProvider_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UTimecodeProvider,
		(UObject* (*)())Z_Construct_UPackage__Script_RenderStream,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URenderStreamTimecodeProvider_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Read timecode from the RenderStream connection.\n */" },
		{ "IncludePath", "RenderStreamTimecodeProvider.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/RenderStreamTimecodeProvider.h" },
		{ "ToolTip", "Read timecode from the RenderStream connection." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_URenderStreamTimecodeProvider_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URenderStreamTimecodeProvider>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URenderStreamTimecodeProvider_Statics::ClassParams = {
		&URenderStreamTimecodeProvider::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_URenderStreamTimecodeProvider_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URenderStreamTimecodeProvider_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URenderStreamTimecodeProvider()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URenderStreamTimecodeProvider_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URenderStreamTimecodeProvider, 3433984632);
	template<> RENDERSTREAM_API UClass* StaticClass<URenderStreamTimecodeProvider>()
	{
		return URenderStreamTimecodeProvider::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URenderStreamTimecodeProvider(Z_Construct_UClass_URenderStreamTimecodeProvider, &URenderStreamTimecodeProvider::StaticClass, TEXT("/Script/RenderStream"), TEXT("URenderStreamTimecodeProvider"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URenderStreamTimecodeProvider);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
